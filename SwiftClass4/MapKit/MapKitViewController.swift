//
//  MapKitViewController.swift
//  SwiftClass4
//
//  Created by Igor Maldonado Floor on 09/03/18.
//  Copyright © 2018 Igor Maldonado Floor. All rights reserved.
//

import UIKit
import MapKit

class MapKitViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "MapKit"
        
        let initialLocation = CLLocation(latitude: -21.191602, longitude: -47.815394)

        self.centerMapOnLocation(location: initialLocation)
        
        let artwork = Artwork(title: "Universidade Estácio de Sá", locationName: "Rua Abrahão Issa Halach, 980 - Ribeirânia", discipline: "Study", coordinate: CLLocationCoordinate2D(latitude: -21.2081636, longitude: -47.7887213))
        self.mapView.addAnnotation(artwork)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let regionRadius: CLLocationDistance = 20000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

}
