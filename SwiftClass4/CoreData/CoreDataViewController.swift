//
//  CoreDataViewController.swift
//  SwiftClass4
//
//  Created by Igor Maldonado Floor on 09/03/18.
//  Copyright © 2018 Igor Maldonado Floor. All rights reserved.
//

import UIKit
import CoreData

class CoreDataViewController: UIViewController {
    var cities: [City] = []

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "CoreData"
        self.cities = self.listCities()
        if cities.count == 0 {
            self.addSomeCities()
            self.cities = self.listCities()
        }

        self.tableview.dataSource = self
        self.tableview.register(CityTableViewCell.self, forCellReuseIdentifier: CityTableViewCell.identifier)
        self.tableview.register(UINib(nibName: CityTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: CityTableViewCell.identifier)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func listCities() -> [City] {
        let myDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = myDelegate!.persistentContainer.viewContext
        var result: [City] = []
        let entityName = "City"
        let fetch = NSFetchRequest<City>(entityName: entityName)
        
        do{
            result = try context.fetch(fetch)
        } catch {

        }
        return result
    }
    
    func addSomeCities() {
        let myDelegate = UIApplication.shared.delegate as? AppDelegate
        let context = myDelegate!.persistentContainer.viewContext
        
        let saoPauloCity = City(context: context)
        saoPauloCity.name = "São Paulo"
        saoPauloCity.photoName = "saopaulo"
        
        let cuiabaCity = City(context: context)
        cuiabaCity.name = "Cuiabá"
        cuiabaCity.photoName = "cuiaba"
        
        let rioCity = City(context: context)
        rioCity.name = "Rio De Janeiro"
        rioCity.photoName = "rio"
        
        let manausCity = City(context: context)
        manausCity.name = "Manaus"
        manausCity.photoName = "manaus"
        
        let portoCity = City(context: context)
        portoCity.name = "Porto Alegre"
        portoCity.photoName = "porto"
        
        
        try! context.save()
    }
   
}

extension CoreDataViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableview.dequeueReusableCell(withIdentifier: CityTableViewCell.identifier) as? CityTableViewCell else {
            fatalError("Error dequeuing CityTableViewCell")
        }
        let city = cities[indexPath.row]
        
        cell.setup(city)

        
        return cell
    }
    
    
}
