//
//  CityTableViewCell.swift
//  SwiftClass4
//
//  Created by Igor Maldonado Floor on 09/03/18.
//  Copyright © 2018 Igor Maldonado Floor. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {
    static let identifier = "CityTableViewCell"
    
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var cityImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setup(_ city: City) {
        self.cityNameLabel.text = city.name
        self.imageView?.image = UIImage.init(named: city.photoName ?? "")
    }
}
