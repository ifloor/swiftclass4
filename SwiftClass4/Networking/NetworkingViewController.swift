//
//  NetworkingViewController.swift
//  SwiftClass4
//
//  Created by Igor Maldonado Floor on 09/03/18.
//  Copyright © 2018 Igor Maldonado Floor. All rights reserved.
//

import UIKit

class NetworkingViewController: UIViewController {

    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var label: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Networking"
        self.label.text = "Horário"
        
        self.loadIndicator.isHidden = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - UI actions
    @IBAction func fetchButtonTapped(_ sender: Any) {
        self.loadIndicator.isHidden = false
        self.loadIndicator.startAnimating()
        
        if let url = URL(string: "http://igormaldonado.com.br/files/times/time-igor") {
            do {
                let contents = try String(contentsOf: url)
                self.label.text = "Horário: \(contents)"
                self.loadIndicator.stopAnimating()
                self.loadIndicator.isHidden = true
            } catch {
                // contents could not be loaded
                self.loadIndicator.stopAnimating()
                self.loadIndicator.isHidden = true
                self.label.text = "Horário: Erro"
            }
        } else {
            // the URL was bad!
            self.loadIndicator.stopAnimating()
            self.loadIndicator.isHidden = true
            self.label.text = "Horário: Erro"
        }
    }
    
}
