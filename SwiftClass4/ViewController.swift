//
//  ViewController.swift
//  SwiftClass4
//
//  Created by Igor Maldonado Floor on 09/03/18.
//  Copyright © 2018 Igor Maldonado Floor. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Unidade 4"
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func networkingButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "networkingSegue", sender: nil)
    }
    
    @IBAction func coreDataButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "coreDataSegue", sender: nil)
    }
    
    @IBAction func photoLibraryButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "photoLibrarySegue", sender: nil)
    }
    
    @IBAction func mapKitButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "mapKitSegue", sender: nil)
    }
    
}

